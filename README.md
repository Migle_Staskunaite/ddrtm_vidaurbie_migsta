Vidmantas Aurelijus Bielskus, Miglė Staškūnaiatė

Didžiujų duomenų tyrybos metodai - Projekto žingsniai
Kodo paleidimas
1. Į ktu-mgmf-ubuntu-server-1604\synced_dir įdėti customer_churn_000x  ir customer_usage_000x failus, paleisti per sparko
1. serverio Rstudio pirmass 384 kodo eilutes, kitas eilutes perrašyti į kitą failą (pvz a.R) ir pirmosiom pabaigus skaičiuoti
1. nukopijuoti tą a.R failą į ktu-mgmf-ubuntu-server-1604/synced_dir/projektas/clusters/ iš R aplanko
1. (paprastai būna C:\Program Files\R\R-3.5.1\bin\x64\Rscript.exe) pasidaryti "short-cut" į /synced_dir/projektas/clusters/
1. Rstudio sparklyr serverio terminale parašyti "Rscript b.R parameters.json" kodas turėtų viską paleisti, aplankus sukuria
1. automatiškai.

Užduoties etapai:

1. Duomenų agregavimas ir filtravimas
2. Vartotojų klasterizavimas
3. Vartotojų segmentavimas

1.Duomenų agregavimas ir filtravimas

Pirma buvo įrašytos sparklyr ir dplyr bibliotekos, kad būtų galima dirbti su spark per R studio bei naudotis dplyr duomenų tvarkymo galimybėmis.
Buvo priskirti pavadinimai įvesties ir išvesties failam "path_input_usage" bei "path_output_aggregated_usage" taip pat sukurtas prisijungimas prie spark serverio
"sc". Iš path_input_usage nuskaityti 9 varianto duomenys ir išsaugoti "usage_tbl".
Vėliau iš šio duomenų rinkinio buvo išfiltruoti kintamojo "user_no_outgoing_activity_in_days" duomenys, kurie viršyja reikšmę 60 dienų kaip išskirtys, ir pašalinti iš duomneų rinkinio.
Buvo išrinkti kintamųjų pavadinimai su colnames funkcija, year, month ir user_id buvo atskirti nuo kitų, kad nesusiduplikuotų.
Sekančiame žingsnyje usage_tbl duomenų rinkinys buvo išgrupuotas pagal user_account_id ir kiekvienam iš kintamųjų paskaičiuotos minimaalios, vidutinės bei maksimalios reikšmės,
o rezultatas patalpintas agg_usage_by_customer_tbl duomenų rinkinyje. Naudota na.rm = TRUE parinktis, kad pašalinti not/applicable reikšmes.
Šis duomenų rinkinys buvo įrašytas su spark_write_csv funkcija į išvesties kelią ir prisijungimaas prie spark buvo uždarytas.

2.Vartotojų klasterizavimas

Vėl iškviečiamos sparklyr ir dplyr bibliotekos. Analogiškai priskiriami išvesties ir įvesties keliai. Sukuriamas spark prisijungimas, į "sparką" įrašomas ankščiau buvusiame žingsnyje
sukurtas duomenų rinkinys su vidutinėm, minimaliom ir maksimaliomis kiekvieno kintamojo reikšmėmis reikalingomis k-means algoritmui skaičiuoti. Išrenkami feature_columns kurie vėliau veiks
kaip atstumo nustatymo įrankiai tarp elementų, vėliau yra prijungiami pradiniai duomenys, jie normalizuojami ir patikrinami atstumai tarp klasterių su 3-8 klasteriais. 

3.Vartotojų segmentavimas

Užduočiai atlikti turime prie agreguotų duomenų apjungti `churn` stulpelį iš `customer_churners.csv` failo. Tam reikės nuskaityti abu DataFrame ir atlikti left join funkciją.
Tuomet duomenys bus suskaidyti į training, validation ir testing imtis naudojant `sdf_partition` proporcijomis training (0.7), test (0.2), validatin (0.1) bei išsaugoti kaip naujus DataFrame.  
Turint visą reikiamą informaciją, buvo pritaikytas Random Forest algoritmas klasifikavimui.