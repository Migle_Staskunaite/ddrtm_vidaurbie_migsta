#loading libraries
library(sparklyr, lib.loc="~/R/x86_64-pc-linux-gnu-library/3.4")
library(dplyr, lib.loc="~/R/x86_64-pc-linux-gnu-library/3.4")
library(dbplot)
library(R.utils)
library(ggplot2)
library(jsonlite)
library(magrittr)
#creating necessary diirectories
dir.create("~/synced_dir/projektas")
dir.create("~/synced_dir/projektas/data")
dir.create("~/synced_dir/projektas/histograms")
dir.create("~/synced_dir/projektas/centrai/")
dir.create("~/synced_dir/projektas/clusters/")
dir.create("~/synced_dir/projektas/metrics/")
dir.create("~/synced_dir/projektas/output/")
#starting work
setwd("~/synced_dir/projektas/data")
transpose_df <- function(df_input) {
  df <- as.data.frame(df_input)
  for (c in colnames(df)[-1]) {
    df[[c]] <- as.double(df[[c]])
  }
  cols <- df$summary
  df <- as.data.frame(t(df[,-1]))
  colnames(df) <- cols
  cbind(column=rownames(df), data.frame(df, row.names=NULL))
}

path_input_usage <- "~/synced_dir/customer_usage_0009.csv"
path_output_aggregated_usage <- "~/synced_dir/projektas/aggregated_data"

sc <- spark_connect(master = "local", spark_home = "/opt/apache-spark/")

usage_tbl <- spark_read_csv(sc, "customer_usage", path_input_usage)

summary_usage_raw_df <- sdf_describe(usage_tbl) %>%
  collect()

summary_df <- transpose_df(summary_usage_raw_df)
summary_df
str(summary_df)


columns_used_in_clustering <- c("user_account_id","sms_outgoing_spendings", "sms_outgoing_to_abroad_spendings",  "user_no_outgoing_activity_in_days", "calls_outgoing_to_abroad_spendings", "calls_outgoing_spendings", "gprs_spendings", "user_spendings")
usage_tbl1 <- usage_tbl %>%
  select("user_account_id","sms_outgoing_spendings", "sms_outgoing_to_abroad_spendings",  "user_no_outgoing_activity_in_days", "calls_outgoing_to_abroad_spendings", "calls_outgoing_spendings", "gprs_spendings", "user_spendings")



#  plotting all columns
for (col in columns_used_in_clustering) {
  # notice the use of UQ(sym(x)) where x is the valiable 
  # containing column name, col in this case.
  # this has to be used to dynamically pass the column name to dplot 
  # (or dplyr) function
  histogram_plot <- usage_tbl1 %>% 
    ggplot(aes(UQ(sym(col)))) + geom_histogram(bins = 20)+
    ggtitle(paste0("Histogram of ", col))
  
  print(histogram_plot)
  
  path_histogram <- paste0("~/synced_dir/projektas/histograms/histogram_", col, ".png")
  ggplot2::ggsave(path_histogram, plot = histogram_plot, 
                  width = 12, height = 10, units = "cm")
}
#userspendings var
pct99_sms_outgoing_to_abroad_spendings <- usage_tbl1 %>%
  summarise(
    sms_outgoing_to_abroad_spendings = percentile_approx(sms_outgoing_to_abroad_spendings, 0.99)) %>%
  collect()

pct99_sms_outgoing_to_abroad_spendings
#sms_outgoing_spendings
pct99.5_sms_outgoing_spendings <- usage_tbl1 %>%
  summarise(
    sms_outgoing_spendings = percentile_approx(sms_outgoing_spendings, 0.995)) %>%
  collect()
pct99.5_sms_outgoing_spendings
# user_no_outgoing_activity_in_days 
pct99.5_user_no_outgoing_activity_in_days <- usage_tbl1 %>%
  summarise(
    user_no_outgoing_activity_in_days = percentile_approx(user_no_outgoing_activity_in_days, 0.995)) %>%
  collect()
pct99.5_user_no_outgoing_activity_in_days
#calls_outgoing_to_abroad_spendings"
pct99.5_calls_outgoing_to_abroad_spendings <- usage_tbl1 %>%
  summarise(
    calls_outgoing_to_abroad_spendings = percentile_approx(calls_outgoing_to_abroad_spendings, 0.995)) %>%
  collect()
pct99.5_calls_outgoing_to_abroad_spendings
#calls_outgoing_spendings
pct99.5_calls_outgoing_spendings <- usage_tbl1 %>%
  summarise(
    calls_outgoing_spendings = percentile_approx(calls_outgoing_spendings, 0.995)) %>%
  collect()
pct99.5_calls_outgoing_spendings
#gprs_spendings
pct99_gprs_spendings <- usage_tbl1 %>%
  summarise(
    gprs_spendings = percentile_approx(gprs_spendings, 0.99)) %>%
  collect()
pct99_gprs_spendings
#user_spendings
pct99_user_spendings <- usage_tbl1 %>%
  summarise(
    user_spendings = percentile_approx(user_spendings, 0.99)) %>%
  collect()
pct99_user_spendings
#summarizingpercentiles 
col_percentiles <- list(
  c(sms_outgoing_to_abroad_spendings = 0.99),
  c(sms_outgoing_to_abroad_spendings = 0.0),
  c(sms_outgoing_spendings = 0.995),
  c(sms_outgoing_spendings = 0.005),
  c(user_no_outgoing_activity_in_days = 0.995),
  c(user_no_outgoing_activity_in_days = 0.0),
  c(calls_outgoing_to_abroad_spendings = 0.995),
  c(calls_outgoing_to_abroad_spendings = 0.0),
  c(calls_outgoing_spendings = 0.995),
  c(calls_outgoing_spendings = 0.00),
  c(gprs_spendings = 0.99),
  c(gprs_spendings = 0.01),
  c(user_spendings = 0.99),
  c(user_spendings = 0.01)
)
pct_expressions <- list()
for (pct in col_percentiles) {
  col <- names(pct)
  names(pct) <- NULL
  pct_name <- paste0(col, "_pct", sprintf("%.0f", 100 * pct))
  pct_expressions[[pct_name]] <- quo(percentile_approx(UQ(sym(col)), UQ(pct)))
}
pct_expressions

usage_percentiles <- usage_tbl %>%
  summarise(UQS(pct_expressions)) %>%
  collect() %>% as.data.frame()
#filtering data
for (col_pct in colnames(usage_percentiles)) {
  x <- strsplit(col_pct, "_pct")[[1]]
  col <- x[1]
  p <- as.numeric(x[2]) / 100
  pct <- usage_percentiles[1, col_pct]
  
  # print... or do something else if needed
  print(paste("percentile", p, "of column", col, "is", pct))
}
corrfile <- ml_corr(usage_tbl)
filtered_data <- usage_tbl1 %>%
  filter(between(sms_outgoing_to_abroad_spendings, 0, 4.19))
filtered_data <- filtered_data %>%
  filter(between(sms_outgoing_spendings, 0, 31.83))
filtered_data <- filtered_data %>%
  filter(between(user_no_outgoing_activity_in_days, 0, 1307))
filtered_data <- filtered_data %>%
  filter(between(calls_outgoing_to_abroad_spendings, 0, 15.03))
filtered_data <- filtered_data %>%
  filter(between(calls_outgoing_spendings, 0, 61.81))
filtered_data <- filtered_data %>%
  filter(between(gprs_spendings, 0, 3.8))
filtered_data <- filtered_data %>%
  filter(between(user_spendings, 0, 60.69))
noactivitydatadescribed <- sdf_describe(filtered_data) %>%
  collect()
#aggregation in order to keep the same user ID in same cluster
all_columns <- colnames(filtered_data)
agg_columns <- setdiff(all_columns, c("year", "month", "user_account_id"))
filtered_data <- filtered_data %>%
  group_by(user_account_id) %>%
  summarise_at(
    vars(agg_columns),
    c("mean"), na.rm = TRUE)

spark_write_csv(filtered_data, path_output_aggregated_usage, mode = "overwrite") 

scaler <- ft_standard_scaler(sc, with_mean = TRUE, with_std = TRUE)

# clustering

path_input_aggregated_usage <- "synced_dir/projektas/aggregated_data"

sc <- spark_connect(master = "local", spark_home = "/opt/apache-spark/")

feature_columns <- setdiff(colnames(filtered_data), c("user_account_id"))

round(ml_corr(usage_tbl, columns = feature_columns , method = "pearson"), 2)

usage_vec_tbl <- ft_vector_assembler(filtered_data, feature_columns, "raw_features")
#standardization of data
scaler_model <- ft_standard_scaler(
  sc,
  input_col = "raw_features",
  output_col = "features",
  with_mean = TRUE,
  with_std = TRUE)
scaler <- ml_fit(scaler_model, usage_vec_tbl)

usage_features_tbl <- ml_transform(scaler, usage_vec_tbl)
#creating needed linkss
cluster_nested_json_out="~/synced_dir/projektas/metrics/centers.JSON"
wssse_nested_json_out= "~/synced_dir/projektas/metrics/wssse.JSON"
path_graph_out="~/synced_dir/projektas/metrics/wssse.jpg"
#clustering with 2:10 clusters
cluster_range = 2:10
for (k in cluster_range) {
  kmeans_model <- ml_kmeans(sc, k = k, features_col = "features", max_iter = 100, seed = 666)
  kmeans <- ml_fit(kmeans_model, usage_features_tbl)
  
  wssse <- kmeans$compute_cost(usage_features_tbl)
  
  wssse_nested_json <- toJSON(c(k, wssse), auto_unbox = TRUE)
  if (k == 2) {
    write(wssse_nested_json, wssse_nested_json_out)
  } else {
    write(wssse_nested_json, wssse_nested_json_out, append = TRUE)
  }

}
#wssse
wssse_reiksmes = sapply(readLines(wssse_nested_json_out), fromJSON)
write.csv(wssse_reiksmes, "~/synced_dir/projektas/metrics/wssse.csv")
read.csv("~/synced_dir/projektas/metrics/wssse.csv", header = TRUE)
wssse_reiksmes <- t(wssse_reiksmes)
wssse_matrix = as.data.frame(wssse_reiksmes)

plot_wssse <- ggplot(wssse_matrix, aes(x = V1, y=V2)) + geom_line() +
  xlab("K") +
  ylab("WSSSE") +
  ggtitle("WSSSE by number of clusters K")

ggsave(path_graph_out, plot = plot_wssse, 
       width = 12, height = 10, units = "cm")




# f(k)

sc <- spark_connect(master = "local", spark_home = "/opt/apache-spark/")

path_sse <- "~/synced_dir/projektas/metrics/wssse.JSON"
k_sse_metrics_df <- stream_in(file(path_sse))
k_sse_metrics_df

colnames1 <- c("k", "sse")
colnames(k_sse_metrics_df) <- colnames1

compute_fk <- function(k, sse, prev_sse, dimension) {
  if (k == 1 || prev_sse == 0) {
    return(1)
  }
  
  weight_factor <- function(k, dimension) {
    weight_k2 <- 1 - 3 / (4 * dimension)
    weight_factor_accumulator(weight_k2, k)
  }
  
  weight_factor_accumulator <- function(acc, k) {
    if (k == 2) {
      return(acc)
    }
    weight_factor_accumulator(acc + (1 - acc) / 6, k - 1)
  }
  
  weight = weight_factor(k , dimension)
  sse / (weight * prev_sse)
}
mk_fk_args_df <- function(k_sse_df) {
  n <- nrow(k_sse_df)
  prev_sse <- c(0, k_sse_df$sse[-n])
  prev_k <-c(1, k_sse_df$k[-n])
  df <- transform(k_sse_df, prev_k = prev_k, prev_sse = prev_sse)
  df[df$k - df$prev_k == 1, c("k", "sse", "prev_sse")]
}

compute_fk_df <- function(k_sse_df, dimension) {
  # use loops and imperative style
  df <- mk_fk_args_df(k_sse_df)
  n <- nrow(df)
  fk <- numeric(n)
  for (i in 1:n) {
    fk[i] <- compute_fk(df$k[i], df$sse[i], df$prev_sse[i], dimension)
  }
  data.frame(k = df$k, sse = df$sse, fk = fk)
}

compute_fk_df_v <- function(k_sse_df, dimension) {
  # vectorize compute_fw with a wrapper and use dplyr
  compute_fk_v <- Vectorize(compute_fk)
  k_see_prev_sse_df <- mk_fk_args_df(k_sse_df)
  k_see_prev_sse_df %>%
    transmute(k = k, sse = sse, f_k = compute_fk_v(k, sse, prev_sse, dimension))
}

# dimension of data that was used in KMeans clustering
# when WSSSE metric values were obtained
data_dimension <- 2

fk_df <- compute_fk_df(k_sse_metrics_df, data_dimension)

plot_sse_s <- ggplot(fk_df, aes(x=k, y=sse)) + geom_line() +
  xlab("K") +
  ylab("WSSSE") +
  ggtitle("WSSSE by number of clusters K") +
  theme_bw()

plot_fk <-  ggplot(fk_df, aes(x=k, y=fk)) + geom_line() +
  xlab("K") +
  ylab("f(K)") +
  ggtitle("f(K) by number of clusters K") +
  theme_bw()

plot_sse_s
plot_fk
sdd <- sd(fk_df[,"fk"])
for(i in 1:8) {
  if(fk_df[i,3] > fk_df[i+1,3]+fk_df[i+1,3]*0.3*sdd) {
    k <- (fk_df[i,1])
    break;
  }
}
#finalizing clustering with optimal number of clusters
kmeans_model <- ml_kmeans(sc, k = k, features_col = "features", max_iter = 100)
kmeans <- ml_fit(kmeans_model, usage_features_tbl)
centers <- kmeans$cluster_centers()
kmeans$summary$cluster_sizes()
export_json1 <- toJSON(centers, pretty=TRUE, auto_unbox=TRUE)
write(export_json1, "centers.json")
prediction <- ml_predict(kmeans, usage_features_tbl)
ml_clustering_evaluator(prediction)
temp = kmeans$summary$predictions %>%
  select(
    setdiff(colnames(kmeans$summary$predictions),
            c('raw_features', 'features'))
  )
temp = temp %>%
  rename(cluster = prediction)

csv_path = "~/synced_dir/projektas/centrai/data_clus.csv"
write.csv(temp, file = csv_path, row.names = FALSE)
# test/train
path_input = "~/synced_dir/projektas/centrai/data_clus.csv"
data_clusters <- spark_read_csv(sc, "data_clusters", path_input)

path_input_churn = "~/synced_dir/customer_churn_0009.csv"
data_churn = spark_read_csv(sc, "data_churn", path_input_churn)

#joining churn and usage data
join = data_clusters %>%
  left_join(data_churn, by = c("user_account_id"))

for(i in 1:k){
  assign(paste0("join", i), filter(join, cluster == i-1))
}
#partition data
cluster_1 = sdf_partition(join1, training = 0.7, test = 0.3, seed = 420)
cluster_2 = sdf_partition(join2, training = 0.7, test = 0.3, seed = 420)
cluster_3 = sdf_partition(join3, training = 0.7, test = 0.3, seed = 420)
spark_write_json(cluster_1$test, "~/synced_dir/projektas/clusters/1/test")
spark_write_json(cluster_1$training, "~/synced_dir/projektas/clusters/1/training")
spark_write_json(cluster_2$test, "~/synced_dir/projektas/clusters/2/test")
spark_write_json(cluster_2$training, "~/synced_dir/projektas/clusters/2/training")
spark_write_json(cluster_3$test, "~/synced_dir/projektas/clusters/3/test")
spark_write_json(cluster_3$training, "~/synced_dir/projektas/clusters/3/training")
#create parameters list, creating loops that will write lists according to values of depth,ntree,k
input_training = c()
input_validation = c()
input_test = c()
for (i in 1:k) {
  input_training[i] <- paste0("~/synced_dir/projektas/clusters/", i, "/training/")
}
for (i in 1:k) {
  input_test[i] <- paste0("~/synced_dir/projektas/clusters/", i, "/test/")
}
for (i in 1:k) {
  dir.create(paste0("~/synced_dir/projektas/output/", i))
}
ntree <- c(15,35,55,75)
depth <- c(5,10,15)
parameters <- list(input_training,input_test,depth,ntree)
setwd("~/synced_dir/projektas/clusters/")
export_json <- jsonlite::toJSON(parameters, pretty=TRUE, auto_unbox=TRUE)
write(export_json, "parameters.json")