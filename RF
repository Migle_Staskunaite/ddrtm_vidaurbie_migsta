library(jsonlite)
library(sparklyr)
library(dplyr)
sc <- spark_connect(master = "local", spark_home = "/opt/apache-spark/")
args <- commandArgs(trailingOnly = TRUE)

if (length(args) != 1) {
  msg <- "Usage: spark-submit [options] some_parametrized_job.R PATH_PARAMS"
  write(msg, stderr())
  quit(status = 1)
}
library(uuid)

jp <- read_json(args[1], simplifyVector = TRUE)
path_training <- jp[1]
path_test <- jp[2]
path_depth <- jp[3]
path_ntree <- jp[4]
print(path_training)
print(path_test)
print("spark job params:")
print(jp)

depth  <- as.list(jp[3])
print(paste0("depth:", depth))
ntree <- as.list(jp[4])
print("ntree")
n_depth <- length(depth[[1]])
nrowtree <- length(ntree[[1]])
k =3 
print("reading training data...")
training_data1 <- spark_read_json(sc, paste0("training1"), path_training[[1]][1])
print("reading testing data...")
test_data1 <- spark_read_json(sc, paste0("test1"), path_test[[1]][1])

#kappametric selfmade
kappametric <- function(ConfMatrix) {
  if (nrow(ConfMatrix) == ncol(ConfMatrix)) {
    #noideawhich is groundtruth and which is classifier but it doesnt matter for calc kappa
    n <- nrow(ConfMatrix)
    matrixofrowsandcolssums <- matrix(nrow = n, ncol = 2) 
    #rowsums
    for (i in 1:ncol(ConfMatrix)) {
      matrixofrowsandcolssums[i,1] <- sum(ConfMatrix[i,])
      
    }
    #colsums
    for (i in 1:n) {
      matrixofrowsandcolssums[i,2] <- sum(ConfMatrix[,i])
    }
    #observed accuracy
    diagsum <- 0
    for (i in 1:n) {
      diagsum <- diagsum + ConfMatrix[i,i]
      if (i == n) {
        ObservedAccuracy <- (diagsum/sum(matrixofrowsandcolssums[,1]))
        break;
      }
    }
    #Expected Accuracy
    #Marginal Frequency Vector
    MFV <- matrix(nrow = n, ncol = 1)
    for (i in 1:n) {
      MFV[i,] <- sum((sum(ConfMatrix[i,] * sum(ConfMatrix[,i]))))/sum(ConfMatrix)
    }
    z = 0
    for (i in 1:n) {
      z <- z + MFV[i,]
      if (i == n)
        ExpectedAccuracy <- z/sum(ConfMatrix)
    }
    Kappa = (ObservedAccuracy - ExpectedAccuracy)/(1-ExpectedAccuracy)
    return(Kappa);
  }
  else {
    print("All predictions were predicted as one class, sign of bad model for imbalanced data")
  }
}
#metrics is matrix to which we store metrics, rows is total iterations number, columns is number of cols
Metrics1 <- data.frame(nrow = length(depth[[1]])*length(ntree[[1]]), ncol = 7)
#segment1
x = 0
for (z in 1:nrowtree) {
  for (j in 1:n_depth) {
    x <- x + 1
    model_uuid <- UUIDgenerate(use.time = TRUE)
    model_description <- paste0(
      "ntree_", ntree[[1]][z],
      "_ndepth_", depth[[1]][j])
    dir_model_output <- file.path("~/synced_dir/projektas/output/1/", model_description)
    dir.create(dir_model_output)
    path_metrics <- file.path(dir_model_output, "metrics.json")
    path_model <- file.path(dir_model_output, "model.json")
    path_predictions <- file.path(dir_model_output, "churn_prediction.json")
    #training and predicting
    training_data1 <- training_data1 %>%
      select(c(churn ,sms_outgoing_spendings ,sms_outgoing_to_abroad_spendings , user_no_outgoing_activity_in_days ,calls_outgoing_to_abroad_spendings , calls_outgoing_spendings , gprs_spendings , user_spendings))
    print("running RF model")
    modelRF <- training_data1 %>%
      ml_random_forest_classifier(churn ~ ., num_trees =ntree[[1]][z], max_depth = depth[[1]][j])
    predictions <- ml_predict(modelRF, test_data1)
    #metrics
    print("calculating metrics")
    A <- data.frame(predictions = unlist(collect(select(predictions,prediction))), actuals = unlist(collect(select(test_data1, churn))))
    AA <- table(A$predictions, A$actuals)
    kapp <- kappametric(AA)
    acc <- ml_multiclass_classification_evaluator(predictions, metric_name = "accuracy")
    f1 <- ml_multiclass_classification_evaluator(predictions)
    areaunderROC <- ml_binary_classification_evaluator(predictions, metric_name ="areaUnderROC")
    precision <- ml_multiclass_classification_evaluator(predictions, metric_name = "weightedPrecision")
    recall <- ml_multiclass_classification_evaluator(predictions, metric_name = "weightedRecall")
    Metrics1[x,1] <- model_description
    Metrics1[x,2] <- kapp
    Metrics1[x,3] <- acc
    Metrics1[x,4] <- f1
    Metrics1[x,5] <- areaunderROC
    Metrics1[x,6] <- precision
    Metrics1[x,7] <- recall
    write_json(Metrics1[x,], path_metrics, mode = "append")
    ml_save(modelRF, path_model)
    write_json(toJSON(as.data.frame(AA)), path_predictions)
  }
}
print(Metrics1)
print("iterating segment2")
print("reading training data...")
training_data2 <- spark_read_json(sc, paste0("training2"), path_training[[1]][2])
print("reading testing data...")
test_data2 <- spark_read_json(sc, paste0("test2"), path_test[[1]][2])
Metrics2 <- data.frame(nrow = length(depth[[1]])*length(ntree[[1]]), ncol = 7)
#segment2
x = 0
for (z in 1:nrowtree) {
  for (j in 1:n_depth) {
    x <- x + 1
    model_uuid <- UUIDgenerate(use.time = TRUE)
    model_description <- paste0(
      "ntree_", ntree[[1]][z],
      "_ndepth_", depth[[1]][j])
    dir_model_output <- file.path("~/synced_dir/projektas/output/2/", model_description)
    dir.create(dir_model_output)
    path_metrics <- file.path(dir_model_output, "metrics.json")
    path_model <- file.path(dir_model_output, "model.json")
    path_predictions <- file.path(dir_model_output, "churn_prediction.json")
    #training and predicting
    training_data2 <- training_data2 %>%
      select(c(churn ,sms_outgoing_spendings ,sms_outgoing_to_abroad_spendings , user_no_outgoing_activity_in_days ,calls_outgoing_to_abroad_spendings , calls_outgoing_spendings , gprs_spendings , user_spendings))
    print("running RF model")
    modelRF <- training_data2 %>%
      ml_random_forest_classifier(churn ~ ., num_trees =ntree[[1]][z], max_depth = depth[[1]][j])
    predictions <- ml_predict(modelRF, test_data2)
    #metrics
    print("calculating metrics")
    A <- data.frame(predictions = unlist(collect(select(predictions,prediction))), actuals = unlist(collect(select(test_data2, churn))))
    AA <- table(A$predictions, A$actuals)
    kapp <- kappametric(AA)
    acc <- ml_multiclass_classification_evaluator(predictions, metric_name = "accuracy")
    f1 <- ml_multiclass_classification_evaluator(predictions)
    areaunderROC <- ml_binary_classification_evaluator(predictions, metric_name ="areaUnderROC")
    precision <- ml_multiclass_classification_evaluator(predictions, metric_name = "weightedPrecision")
    recall <- ml_multiclass_classification_evaluator(predictions, metric_name = "weightedRecall")
    Metrics2[x,1] <- model_description
    Metrics2[x,2] <- kapp
    Metrics2[x,3] <- acc
    Metrics2[x,4] <- f1
    Metrics2[x,5] <- areaunderROC
    Metrics2[x,6] <- precision
    Metrics2[x,7] <- recall
    write_json(Metrics2[x,], path_metrics, mode = "append")
    ml_save(modelRF, path_model)
    write_json(toJSON(as.data.frame(AA)), path_predictions)
  }
}
print(Metrics2)
print("iterating segment3")
print("reading training data...")
training_data3 <- spark_read_json(sc, paste0("training3"), path_training[[1]][3])
print("reading testing data...")
test_data3 <- spark_read_json(sc, paste0("test3"), path_test[[1]][3])
Metrics3 <- data.frame(nrow = length(depth[[1]])*length(ntree[[1]]), ncol = 7)
#segment3
segment3c <- filter(training_data3, churn == 1)
segment3nc <- filter(training_data3, churn == 0)
dimc <- sdf_dim(segment3c)
dimnc <- sdf_dim(segment3nc)
sliced <- sdf_sample(segment3nc, fraction = dimc/dimnc)
training_data3 <- sdf_bind_rows(sliced,segment3c)
x = 0
for (z in 1:nrowtree) {
  for (j in 1:n_depth) {
    x <- x + 1
    model_uuid <- UUIDgenerate(use.time = TRUE)
    model_description <- paste0(
      "ntree_", ntree[[1]][z],
      "_ndepth_", depth[[1]][j])
    dir_model_output <- file.path("~/synced_dir/projektas/output/3/", model_description)
    dir.create(dir_model_output)
    path_metrics <- file.path(dir_model_output, "metrics.json")
    path_model <- file.path(dir_model_output, "model.json")
    path_predictions <- file.path(dir_model_output, "churn_prediction.json")
    #training and predicting
    training_data3 <- training_data3 %>%
      select(c(churn ,sms_outgoing_spendings ,sms_outgoing_to_abroad_spendings , user_no_outgoing_activity_in_days ,calls_outgoing_to_abroad_spendings , calls_outgoing_spendings , gprs_spendings , user_spendings))
    print("running RF model")
    modelRF <- training_data3 %>%
      ml_random_forest_classifier(churn ~ ., num_trees =ntree[[1]][z], max_depth = depth[[1]][j])
    predictions <- ml_predict(modelRF, test_data3)
    #metrics
    print("calculating metrics")
    A <- data.frame(predictions = unlist(collect(select(predictions,prediction))), actuals = unlist(collect(select(test_data3, churn))))
    AA <- table(A$predictions, A$actuals)
    kapp <- kappametric(AA)
    acc <- ml_multiclass_classification_evaluator(predictions, metric_name = "accuracy")
    f1 <- ml_multiclass_classification_evaluator(predictions)
    areaunderROC <- ml_binary_classification_evaluator(predictions, metric_name ="areaUnderROC")
    precision <- ml_multiclass_classification_evaluator(predictions, metric_name = "weightedPrecision")
    recall <- ml_multiclass_classification_evaluator(predictions, metric_name = "weightedRecall")
    Metrics3[x,1] <- model_description
    Metrics3[x,2] <- kapp
    Metrics3[x,3] <- acc
    Metrics3[x,4] <- f1
    Metrics3[x,5] <- areaunderROC
    Metrics3[x,6] <- precision
    Metrics3[x,7] <- recall
    write_json(Metrics3[x,], path_metrics, mode = "append")
    ml_save(modelRF, path_model)
    write_json(toJSON(as.data.frame(AA)), path_predictions)
  }
}
print(Metrics3)
